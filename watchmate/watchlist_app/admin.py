from django.contrib import admin
from .models import Video, StreamPlatform, Review

# Register your models here.
admin.site.register(Video)
admin.site.register(StreamPlatform)
admin.site.register(Review)