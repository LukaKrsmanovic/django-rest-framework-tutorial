from rest_framework import serializers
from ..models import Video, StreamPlatform, Review


#   ===   ModelSerializer   ===   #

class ReviewSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Review
        exclude = ['video']
        # fields = "__all__"


class VideoSerializer(serializers.ModelSerializer):
    reviews = ReviewSerializer(many=True, read_only=True)
    
    class Meta:
        model = Video
        fields = "__all__"


class StreamPlatformSerializer(serializers.ModelSerializer):
    videos = VideoSerializer(many=True, read_only=True)
    
    class Meta:
        model = StreamPlatform
        fields = "__all__"



#   ===   Manually written serializer   ===   #


# # Function for validators level validation
# def name_length(value):
#     if len(value) < 2:
#         raise serializers.ValidationError("Name field muse have more than 2 characters")

# class MovieSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     name = serializers.CharField(validators=[name_length])
#     description = serializers.CharField()
#     active = serializers.BooleanField()
    
#     def create(self, validated_data):
#         return Movie.objects.create(**validated_data)
    
#     def update(self, instance, validated_data):
#         instance.name = validated_data.get('name', instance.name)
#         instance.description = validated_data.get('description', instance.description)
#         instance.active = validated_data.get('active', instance.active)
#         instance.save()
#         return instance
        
#     # # Object level validation
#     # def validate(self, data):
#     #     if len(data['name']) < 2:
#     #         raise serializers.ValidationError("Name field must have more than 2 characters")
#     #     return data
        
#     # # Field level validation
#     # def validate_name(self, value):
#     #     if len(value) < 2:
#     #         raise serializers.ValidationError("Name field must have more than 2 characters")
#     #     return value