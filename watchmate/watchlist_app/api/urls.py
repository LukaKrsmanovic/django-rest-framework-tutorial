from django.urls import path, include
# from .views import movie_list, movie_details
from .views import VideoList, VideoDetails, \
                    StreamPlatformList, StreamPlatformDetails, \
                    ReviewViewSet
# from .views import ReviewList, ReviewDetails, ReviewCreate
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('reviews', ReviewViewSet, basename='review')

urlpatterns = [
    path('list/', VideoList.as_view(), name='video-list'),
    path('list/<int:pk>', VideoDetails.as_view(), name='video-details'),
    
    path('', include(router.urls)),
    
    # path('list/<int:pk>/reviews/', ReviewList.as_view(), name='review-list'),
    # path('list/<int:pk>/review-create/', ReviewCreate.as_view(), name='review-create'),
    # path('reviews/<int:pk>', ReviewDetails.as_view(), name='review-details'),
    
    path('streaming-platforms/', StreamPlatformList.as_view(), name='stream-platform-list'),
    path('streaming-platforms/<int:pk>', StreamPlatformDetails.as_view(), name='stream-platform-details'),
]
