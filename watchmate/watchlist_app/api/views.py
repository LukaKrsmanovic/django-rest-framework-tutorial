from ..models import Video, StreamPlatform, Review
from .serializers import VideoSerializer, StreamPlatformSerializer, ReviewSerializer
from rest_framework.response import Response
from rest_framework import status, mixins, generics
# from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework import viewsets
from django.shortcuts import get_object_or_404


#    ===   Class Based Views - ViewSet   ===   #

class ReviewViewSet(viewsets.ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer


#    ===   Class Based Views - ViewSet   ===   #

# class ReviewViewSet(viewsets.ViewSet):
    
#     def list(self, request):
#         queryset = Review.objects.all()
#         serializer = ReviewSerializer(queryset, many=True)
#         return Response(serializer.data)

#     def retrieve(self, request, pk=None):
#         queryset = Review.objects.all()
#         review = get_object_or_404(queryset, pk=pk)
#         serializer = ReviewSerializer(review)
#         return Response(serializer.data)



#    ===   Class Based Views - Concrete View   ===   #

# class ReviewCreate(generics.CreateAPIView):
#     serializer_class = ReviewSerializer
    
#     def perform_create(self, serializer):
#         pk = self.kwargs.get('pk')
#         video = Video.objects.get(pk = pk)
        
#         serializer.save(video = video)


# class ReviewList(generics.ListAPIView):
#     # queryset = Review.objects.all()
#     serializer_class = ReviewSerializer
    
#     def get_queryset(self):
#         pk = self.kwargs['pk']
#         return Review.objects.filter(video = pk)
    
    
# class ReviewDetails(generics.RetrieveUpdateDestroyAPIView):
#     queryset = Review.objects.all()
#     serializer_class = ReviewSerializer



#    ===   Class Based Views - Mixins   ===   #

# class ReviewList(mixins.ListModelMixin, 
#                  mixins.CreateModelMixin, 
#                  generics.GenericAPIView):
#     queryset = Review.objects.all()
#     serializer_class = ReviewSerializer
    
#     def get(self, request, *args, **kwargs):
#         return self.list(request, *args, **kwargs)

#     def post(self, request, *args, **kwargs):
#         return self.create(request, *args, **kwargs)


# class ReviewDetails(mixins.RetrieveModelMixin, 
#                     mixins.UpdateModelMixin,
#                     mixins.DestroyModelMixin,
#                     generics.GenericAPIView):
#     queryset = Review.objects.all()
#     serializer_class = ReviewSerializer

#     def get(self, request, *args, **kwargs):
#         return self.retrieve(request, *args, **kwargs)

#     def put(self, request, *args, **kwargs):
#         return self.update(request, *args, **kwargs)

#     def delete(self, request, *args, **kwargs):
#         return self.destroy(request, *args, **kwargs)



#    ===   Class Based Views - Manually   ===   #

# class ReviewList(APIView):
    
#     def get(self, request):
#         reviews = Review.objects.all()
#         serializer = ReviewSerializer(reviews, many=True)
#         return Response(serializer.data)
    
#     def post(self, request):
#         serializer = ReviewSerializer(data = request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors)


# class ReviewDetails(APIView):
    
#     def get(self, request, pk):
#         review = Review.objects.get(pk = pk)
#         serializer = ReviewSerializer(review)
#         return Response(serializer.data)
    
#     def put(self, request, pk):
#         review = Review.objects.get(pk = pk)
#         serializer = ReviewSerializer(review, data = request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors)
    
#     def delete(self, request, pk):
#         review = Review.objects.get(pk = pk)
#         review.delete()
#         return Response({
#             'message': 'Review deleted'
#         }, status = status.HTTP_204_NO_CONTENT)


#    ===   Class Based Views   ===   #

class StreamPlatformList(APIView):
    
    def get(self, request):
        platforms = StreamPlatform.objects.all()
        serializer = StreamPlatformSerializer(platforms, many = True)
        return Response(serializer.data)
    
    def post(self, request):
        serializer = StreamPlatformSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)
    
    
class StreamPlatformDetails(APIView):
    
    def get(self, request, pk):
        platform = StreamPlatform.objects.get(pk = pk)
        serializer = StreamPlatformSerializer(platform)
        return Response(serializer.data)
    
    def put(self, request, pk):
        platform = StreamPlatform.objects.get(pk = pk)
        serializer = StreamPlatformSerializer(platform, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)
    
    def delete(self, request, pk):
        platform = StreamPlatform.objects.get(pk = pk)
        platform.delete()
        return Response({
            'message': 'Streaming platform deleted'
        }, status = status.HTTP_204_NO_CONTENT)
        


class VideoList(APIView):
    
    def get(self, request):
        videos = Video.objects.all()
        serializer = VideoSerializer(videos, many = True)
        return Response(serializer.data)
    
    def post(self, request):
        serializer = VideoSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)


class VideoDetails(APIView):
    
    def get(self, request, pk):
        video = Video.objects.get(pk = pk)
        serializer = VideoSerializer(video)
        return Response(serializer.data)
    
    def put(self, request, pk):
        video = Video.objects.get(pk = pk)
        serializer = VideoSerializer(video, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)
    
    def delete(self, request, pk):
        video = Video.objects.get(pk = pk)
        video.delete()
        return Response({
            'message': 'Video deleted',
        }, status = status.HTTP_204_NO_CONTENT)


#    ===   Function Based Views   ===   #

# @api_view(['GET', 'POST'])
# def movie_list(request):
#     if request.method == 'GET':
#         movies = Movie.objects.all()
#         serializer = MovieSerializer(movies, many = True)
#         return Response(serializer.data)
    
#     if request.method == 'POST':
#         serializer = MovieSerializer(data = request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         else:
#             return Response(serializer.errors)

# @api_view(['GET', 'PUT', 'DELETE'])
# def movie_details(request, pk):
#     if request.method == 'GET':
#         try:
#             movie = Movie.objects.get(pk=pk)
#         except Movie.DoesNotExist:
#             return Response({
#                 'error': 'Movie not found',
#             }, status = status.HTTP_404_NOT_FOUND)
            
#         serializer = MovieSerializer(movie)
#         return Response(serializer.data)
    
#     if request.method == 'PUT':
#         movie = Movie.objects.get(pk=pk)
#         serializer = MovieSerializer(movie, data = request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         else:
#             return Response(serializer.errors)
        
#     if request.method == 'DELETE':
#         movie = Movie.objects.get(pk=pk)
#         movie.delete()
#         return Response({
#             'message': 'Movie deleted',
#         }, status = status.HTTP_204_NO_CONTENT)
